/*
 * Name: UUT.c
 * Desc: Example code for 64-bit Linux OS on x86_64 Intel systems.  Outputs a message to the command line and calls
 * a function at specified address using two different methods.
 * Author: Reuben Johnston, reub@jhu.edu
 * Course: JHU-ISI, Software Vulnerability Analysis, EN.650.660
 * Notes:
 *   * Set DEBUG in the build preprocessor defines (i.e., -DDEBUG) to 1 to enable debugging prints
 *   * Set one of the following modes using the build preprocessor defines
 *     * CBASEDCALL (i.e., -DCBASEDCALL)
 *     * INLINEASM (i.e., -DINLINEASM)
 * Usage:
 *   $ ./UUT <messageToDisplay> <functionAddr>
 * Copyright:
 *   Reuben Johnston, www.reubenjohnston.com
 */

#include <stdio.h>
#include <stdlib.h>

#define main real_main //This will overload main and use the wrapper program’s main function;  comment out for normal build and remove the wrapper program from the build

int main(int argc, char *argv[]) {
	/*
	 * This function inputs the user specified command line arguments and calls function at specified functionaddr.
	 */
	puts(argv[1]); /* prints message provided via shell to the console */

    unsigned long long int functionaddr;//variable for the function address

    //checks the usage for calling UUT
    if (argc != 3) {
    	printf("Warning, incorrect args. Usage is $ ./UUT <messageToDisplay> <functionAddr>\n");
    	return(EXIT_FAILURE);
    }

    sscanf(argv[2],"%llx",&functionaddr);//input from the command line argument string into a local integer variable for the address

    //C-based alternative to call functionaddr
#ifdef CBASEDCALL
	unsigned long long int (*fptr)() = functionaddr;//create a fptr to call the function at specified address
	fptr();//call the function
#endif

    //Inline assembly-based alternative to call functionaddr
#ifdef INLINEASM
	asm(".intel_syntax noprefix;");
	register unsigned long rax asm("rax");
	rax=(void(*))functionaddr;
	asm("call rax;");
#endif

	return EXIT_SUCCESS;
}
